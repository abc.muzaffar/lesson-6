class Person{
    constructor(name,age){
        this.name = name
        this.age = age
    }
    describe(){
        return `${this.name}, ${this.age} years old`
    }
} 

const jack = new Person("Jack",25)
const jill = new Person("Jill",24)

// console.log(jack.describe());
// console.log(jill.describe());