class Person{
    constructor(name,age){
        this.name = name
        this.age = age
    }
}

class Baby extends Person{
    constructor(name,age,favoriteToy){
        super(name,age)
        this.favoriteToy = favoriteToy
    }
    play(){
        return `${this.name} is ${this.age} years old and playing with ${this.favoriteToy}`
    }
} 

// const ahmad = new Baby("Ahmad",1,"dendy")
// const solih = new Baby("Solih",2,"cat")

// console.log(ahmad.play());
// console.log(solih.play());